package ca.cegepgarneau.tp2appmobile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ca.cegepgarneau.tp2appmobile.db.data.DataClass

class SharedViewModel : ViewModel() {
    val dataList = MutableLiveData<List<DataClass>>()
    val prixPanier = MutableLiveData<Int>() // Use MutableLiveData for Double

    // Function to update the prixPanier value
    fun updatePrixPanier(price: Int) {
        prixPanier.value = price
    }

    val isAdmin = MutableLiveData<Boolean>().apply {
        value = false
    }
}