package ca.cegepgarneau.tp2appmobile.db.data

import java.util.Random

data class DataModel(
    var id : Int = getAutoId(),
    var dataTitle:String,
    var dataDescription:String,
    var dataPrice : Int,
    var dataCategory: Int,
    var dataQuantity: Int){
    companion object{
        fun getAutoId():Int{
            val rdm = Random()
            return rdm.nextInt(100)
        }
    }
}

