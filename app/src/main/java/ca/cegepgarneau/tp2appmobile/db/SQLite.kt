package ca.cegepgarneau.tp2appmobile.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.media.audiofx.AudioEffect.Descriptor
import ca.cegepgarneau.tp2appmobile.db.data.DataModel

class SQLite(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "tp2.db"
        private const val TBL_VENDEURS = "tbl_vendeurs"
        private const val ID = "id"
        private const val NAME = "name"
        private const val DESC = "description"
        private const val PRICE = "price"
        private const val CATEGORY = "category"
        private const val QUANTITY = "quantity"

    }

    override fun onCreate(db: SQLiteDatabase?) {

        val createTblVendeurs = ("CREATE TABLE " + TBL_VENDEURS + " (" +
                ID + " INTEGER PRIMARY KEY, " +
                NAME + " TEXT, " + DESC + " TEXT, " + PRICE + " NUMBER, " +
                CATEGORY + " NUMBER, " + QUANTITY + " NUMBER" + ")")
        db?.execSQL(createTblVendeurs)

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS $TBL_VENDEURS")
        onCreate(db)
    }

    fun insertVendeur(vdr: DataModel): Long {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(ID, vdr.id)
        contentValues.put(NAME, vdr.dataTitle)
        contentValues.put(DESC, vdr.dataDescription)
        contentValues.put(CATEGORY, vdr.dataCategory)
        contentValues.put(PRICE, vdr.dataPrice)
        contentValues.put(QUANTITY, vdr.dataQuantity)

        val success = db.insert(TBL_VENDEURS, null, contentValues)
        db.close()
        return success
    }

    fun getAllVendeurs(): ArrayList<DataModel> {
        val vdrList = ArrayList<DataModel>()
        val selectQuery = "SELECT * FROM $TBL_VENDEURS"
        val db = this.readableDatabase

        val cursor: Cursor?

        try {
            cursor = db.rawQuery(selectQuery, null)
        } catch (e: Exception) {
            e.printStackTrace()
            db.execSQL(selectQuery)
            return ArrayList()
        }

        var id: Int
        var name: String
        var desc: String
        var qty: Int
        var category: Int
        var price: Int

        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(cursor.getColumnIndex("id"))
                name = cursor.getString(cursor.getColumnIndex("name"))
                desc = cursor.getString(cursor.getColumnIndex("description"))
                category = cursor.getInt(cursor.getColumnIndex("category"))
                price = cursor.getInt(cursor.getColumnIndex("price"))
                qty = cursor.getInt(cursor.getColumnIndex("quantity"))

                val vdr = DataModel(
                    id = id, dataTitle = name, dataDescription = desc,
                    dataCategory = category, dataPrice = price, dataQuantity = qty
                )
                vdrList.add(vdr)
            } while (cursor.moveToNext())
        }
        return vdrList

    }

}