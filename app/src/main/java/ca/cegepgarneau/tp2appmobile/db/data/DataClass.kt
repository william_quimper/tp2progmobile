package ca.cegepgarneau.tp2appmobile.db.data

data class DataClass(var dataImage:Int,
                     var dataTitle:String,
                     var dataDescription:String,
                     var dataPrice : Int,
                     var dataCategory: Int,
                     var dataQuantity: Int)
