package ca.cegepgarneau.tp2appmobile.ui.admin

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Switch
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import ca.cegepgarneau.tp2appmobile.R
import ca.cegepgarneau.tp2appmobile.SharedViewModel

class AdminFragment : Fragment() {

    private lateinit var sharedViewModel: SharedViewModel // Référence au SharedViewModel
    private lateinit var switchAdmin: Switch
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Obtenir une référence au SharedViewModel depuis l'activité parente
        sharedViewModel = ViewModelProvider(requireActivity()).get(SharedViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_admin, container, false)

        // Référence au Switch dans votre layout
        switchAdmin = view.findViewById(R.id.switchAdmin)

        // Observer la valeur de isAdmin
        sharedViewModel.isAdmin.observe(viewLifecycleOwner) { isAdmin ->
            // Activer ou désactiver le Switch en fonction de la valeur de isAdmin
            switchAdmin.isChecked = isAdmin
        }

        // Mettre à jour le SharedViewModel lorsque le bouton de confirmation est cliqué
        view.findViewById<Button>(R.id.confirmButton).setOnClickListener {
            val isAdmin = switchAdmin.isChecked // Obtenir l'état actuel du Switch
            sharedViewModel.isAdmin.value = isAdmin // Mettre à jour la valeur dans le SharedViewModel


        }
        return view
    }

}