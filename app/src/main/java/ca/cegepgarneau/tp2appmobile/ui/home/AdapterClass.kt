package ca.cegepgarneau.tp2appmobile.ui.home
import android.view.LayoutInflater
import ca.cegepgarneau.tp2appmobile.R
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView.OnItemClickListener
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ca.cegepgarneau.tp2appmobile.db.data.DataClass

class AdapterClass(private val dataList: ArrayList<DataClass>, private val listener: OnItemClickListener) :
    RecyclerView.Adapter<AdapterClass.ViewHolderClass>() {

    interface OnItemClickListener {
        fun onItemClick(position: Int)
        fun onItemLongClick(position: Int, view: View)
    }


    class ViewHolderClass(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val rvImage: ImageView = itemView.findViewById(R.id.image)
        val rvTitle: TextView = itemView.findViewById(R.id.title)
        val rvDescription: TextView = itemView.findViewById(R.id.description)
        val rvPrice: TextView = itemView.findViewById(R.id.price)
        val rvQuantity: TextView = itemView.findViewById(R.id.quantity)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderClass {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)
        return ViewHolderClass(itemView)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolderClass, position: Int) {
        val currentItem = dataList[position]
        holder.rvImage.setImageResource(currentItem.dataImage)
        holder.rvTitle.text = "Titre : " + currentItem.dataTitle
        holder.rvDescription.text = "Description : " + currentItem.dataDescription
        holder.rvPrice.text = "Prix : " + currentItem.dataPrice.toString()
        holder.rvQuantity.text = "Quantité : " + currentItem.dataQuantity.toString()

        // Set a click listener on the entire item view
        holder.itemView.setOnClickListener {
            listener.onItemClick(position)
        }
        holder.itemView.setOnLongClickListener {
            listener.onItemLongClick(position, holder.itemView)
            true // Consume the long-press event
        }
    }
}
