package ca.cegepgarneau.tp2appmobile.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ca.cegepgarneau.tp2appmobile.SharedViewModel
import ca.cegepgarneau.tp2appmobile.databinding.FragmentDashboardBinding
import ca.cegepgarneau.tp2appmobile.databinding.FragmentHomeBinding
import ca.cegepgarneau.tp2appmobile.db.data.DataClass
import ca.cegepgarneau.tp2appmobile.ui.home.AdapterClass

class DashboardFragment : Fragment(), AdapterClass.OnItemClickListener {

    private var _binding: FragmentDashboardBinding? = null
    private lateinit var recyclerView : RecyclerView
    private lateinit var dataList: ArrayList<DataClass>
    private val sharedViewModel: SharedViewModel by activityViewModels<SharedViewModel>()

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val dashboardViewModel =
            ViewModelProvider(this).get(DashboardViewModel::class.java)

        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        val root: View = binding.root

        recyclerView = binding.recyclerView
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.setHasFixedSize(true)

        dataList = ArrayList<DataClass>()
        getData()


        return root
    }

    override fun onItemClick(position: Int) {
        // Handle the item click here
        val clickedItem = dataList[position]
        // You can do something with the clicked item, like showing a detail view or performing an action.
        val price = clickedItem.dataPrice // Get the price of the clicked item
        val toastMessage = "Price: $price" // Create the toast message

        // Display a toast with the price
        Toast.makeText(context, toastMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onItemLongClick(position: Int, view: View) {
        // Handle the long-press event here
        // You can show a context menu, a dialog, or perform any other action
        // when a long-press event occurs on an item in the RecyclerView
    }

    private fun getData() {
        sharedViewModel.dataList.observe(viewLifecycleOwner) { dataListe ->
            for(data in dataListe){
                val dataElement = DataClass(data.dataImage, data.dataTitle, data.dataDescription, data.dataPrice, data.dataCategory, data.dataQuantity)
                dataList.add(dataElement)
            }
            recyclerView.adapter = AdapterClass(dataList,this)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}