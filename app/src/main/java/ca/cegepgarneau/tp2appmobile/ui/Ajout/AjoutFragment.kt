package ca.cegepgarneau.tp2appmobile.ui.Ajout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import ca.cegepgarneau.tp2appmobile.R
import ca.cegepgarneau.tp2appmobile.databinding.FragmentAjoutBinding
import ca.cegepgarneau.tp2appmobile.db.SQLite
import ca.cegepgarneau.tp2appmobile.db.data.DataClass
import ca.cegepgarneau.tp2appmobile.db.data.DataModel

class AjoutFragment : Fragment() {
    private var _binding: FragmentAjoutBinding? = null
    private val binding get() = _binding!!
    private lateinit var sqliteHelper: SQLite

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAjoutBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val buttonAdd = binding.buttonOK
        val buttonCancel = binding.buttonCancel

        val editTextTitle = binding.dataTitleEditText
        val editTextDescription = binding.dataDescriptionEditText
        val editTextPrice = binding.dataPriceEditText
        val editTextCategorie = binding.dataCategoryEditText
        val editTextQuantite = binding.dataQuantityEditText

        buttonAdd.setOnClickListener {
            val title = editTextTitle.text.toString()
            val description = editTextDescription.text.toString()
            val priceText = editTextPrice.text.toString()
            val categorieText = editTextCategorie.text.toString()
            val quantiteText = editTextQuantite.text.toString()

            if (title.isEmpty() || description.isEmpty() || priceText.isEmpty() || categorieText.isEmpty() || quantiteText.isEmpty()) {
                // Handle the case where any of the input fields are empty
                Toast.makeText(requireContext(), "Veuillez entrer toutes les informations", Toast.LENGTH_SHORT).show()
            } else {
                try {
                    // Parse the input fields to integers
                    val price = priceText.toInt()
                    val categorie = categorieText.toInt()
                    val quantite = quantiteText.toInt()

//                    // Create the new DataModel instance
//                    val vdr = DataModel(dataTitle = title, dataDescription = description,
//                        dataCategory = categorie, dataPrice = price, dataQuantity = quantite )
//                    val status = sqliteHelper.insertVendeur(vdr)
//                    if (status > -1){
//                        Toast.makeText(context,"Ajouté",Toast.LENGTH_SHORT).show()
//                    }
//                    else{
//                        Toast.makeText(context,"non fonctionné",Toast.LENGTH_SHORT).show()
//                    }

                    val navController = findNavController()

                    // Navigate to the HomeFragment
                    navController.navigate(R.id.action_ajoutFragment_to_navigation_home)

                    // Perform the logic to add newDataItem to your data source
                    // After adding the data, you can navigate to a different fragment or take any other action.
                } catch (e: NumberFormatException) {
                    // Handle the case where the input cannot be parsed to an integer
                    Toast.makeText(requireContext(), "Veuillez entrer des valeurs numériques valides", Toast.LENGTH_SHORT).show()
                }
            }
        }

        buttonCancel.setOnClickListener{
            val navController = findNavController()

            // Navigate to the HomeFragment
            navController.navigate(R.id.action_ajoutFragment_to_navigation_home)
        }

        return root
    }

    private fun getVendeurs(){
        val vdrList = sqliteHelper.getAllVendeurs()

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}

