package ca.cegepgarneau.tp2appmobile.ui.home

import android.app.AlertDialog
import android.os.Bundle
import android.provider.ContactsContract.Data
import android.text.InputType
import android.view.*
import android.widget.EditText
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ca.cegepgarneau.tp2appmobile.databinding.FragmentHomeBinding
import ca.cegepgarneau.tp2appmobile.db.data.DataClass
import ca.cegepgarneau.tp2appmobile.R
import ca.cegepgarneau.tp2appmobile.SharedViewModel

class HomeFragment : Fragment(), AdapterClass.OnItemClickListener {

    private var _binding: FragmentHomeBinding? = null
    private lateinit var recyclerView : RecyclerView
    private lateinit var dataList: ArrayList<DataClass>
    private val sharedViewModel: SharedViewModel by activityViewModels<SharedViewModel>()
    lateinit var imageList:Array<Int>
    lateinit var priceList:Array<Int>
    lateinit var quantityList:Array<Int>
    lateinit var titleList:Array<String>
    lateinit var descList:Array<String>
    lateinit var categoryList:Array<Int>

//    lateinit var dbListItems:ArrayList


    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val homeViewModel =
            ViewModelProvider(this)[HomeViewModel::class.java]

        // Initialize your data
        // ...

        imageList = arrayOf(
            R.drawable.img,
            R.drawable.img_1,
            R.drawable.img_2,
            R.drawable.img_3
        )
        titleList = arrayOf(
            "ttt",
            "ttt",
            "ttt",
            "ttt"
        )
        descList = arrayOf(
            "ttt",
            "ttt",
            "ttt",
            "ttt"
        )
        priceList = arrayOf(
            1,2,3,4
        )
        quantityList = arrayOf(
            1,2,3,4
        )
        categoryList = arrayOf(1,2,3,4)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root



        recyclerView = binding.recyclerView
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.setHasFixedSize(true)

        binding.addButton.setOnClickListener{

            val navController = findNavController()
            // Navigate to the AjoutFragment
            navController.navigate(R.id.action_navigation_home_to_ajoutFragment)
        }

        dataList = ArrayList<DataClass>()
        getData()
        return root
    }

    override fun onItemLongClick(position: Int, view: View) {
        val popupMenu = PopupMenu(context, view)
        popupMenu.menuInflater.inflate(R.menu.popup_menu, popupMenu.menu)

        // Observer the value of isAdmin
        sharedViewModel.isAdmin.observe(viewLifecycleOwner) { isAdmin ->
            // Check if the user is an admin
            popupMenu.menu.findItem(R.id.menu_modify).isVisible = isAdmin
            popupMenu.menu.findItem(R.id.menu_delete).isVisible = isAdmin
        }

        popupMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.menu_delete -> {
                    // Handle delete option
                    // You can access the item to delete from your data source using the position
                    dataList.removeAt(position)
                    if (recyclerView.adapter != null) {
                        recyclerView.adapter!!.notifyItemRemoved(position)
                    }
                    true
                }
                R.id.menu_modify -> {
                    //TODO
                    // Handle modify option
                    true
                }
                else -> false
            }
        }
        popupMenu.show()
    }

    override fun onItemClick(position: Int) {
        // Handle the item click here
        val clickedItem = dataList[position]

        // Create an AlertDialog to prompt the user for item quantity
        val builder = AlertDialog.Builder(requireActivity())
        builder.setTitle("Enter Quantity")

        val input = EditText(requireActivity())
        input.inputType = InputType.TYPE_CLASS_NUMBER
        builder.setView(input)

        builder.setPositiveButton("OK") { dialog, which ->
            val quantityString = input.text.toString()
            if (quantityString.isNotBlank()) {
                val quantity = quantityString.toInt()

                val totalPrice = clickedItem.dataPrice * quantity // Calculate the total price

                // update the cart total price in the shared ViewModel
                sharedViewModel.updatePrixPanier(totalPrice)

                val infoItem = DataClass(0, clickedItem.dataTitle, clickedItem.dataDescription,
                    clickedItem.dataPrice, clickedItem.dataCategory, quantity)

                updateDataList(infoItem)

                // Check the size of the dataList in the shared ViewModel
                val dataListSize = sharedViewModel.dataList.value?.size ?: 0
                Toast.makeText(requireActivity(), "Data list size: $dataListSize", Toast.LENGTH_SHORT).show()
            } else {
                // Handle the case when the user didn't enter a valid quantity.
                Toast.makeText(requireActivity(), "Please enter a valid quantity.", Toast.LENGTH_SHORT).show()
            }
        }

        builder.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun getData(){
        dataList.add(DataClass(0,"titre1","desc1", 10, 1,10))
        dataList.add(DataClass(0,"titre2","desc2", 20, 2,20))
        dataList.add(DataClass(0,"titre3","desc3", 30, 3,30))
        dataList.add(DataClass(0,"titre4","desc4", 40, 4,40))

        for(i in dataList) {
            when (i.dataCategory) {
                1 -> {
                    i.dataImage = R.drawable.img
                }
                2 -> {
                    i.dataImage = R.drawable.img_1
                }
                3 -> {
                    i.dataImage = R.drawable.img_2
                }
                4 -> {
                    i.dataImage = R.drawable.img_3
                }
            }
        }

//        for(i in imageList.indices){
//            val dataClass = DataClass(imageList[i], titleList[i], descList[i], priceList[i], 0, quantityList[i] )
//            dataList.add(dataClass)
//        }
        recyclerView.adapter = AdapterClass(dataList,this)
    }

    // Function to update or initialize the dataList
    private fun updateDataList(newData : DataClass) {
        // Create or fetch your list of DataClass
        val newDataList = mutableListOf<DataClass>()// Fetch or create your list here
        newDataList.add(newData)
        // Get the current value of dataList from the shared ViewModel
        val currentDataList = sharedViewModel.dataList.value.orEmpty()

        // Update the dataList by combining the new data with the existing data
        sharedViewModel.dataList.value = currentDataList + newDataList
    }
}